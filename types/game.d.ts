declare let socket: SocketIOClient.Socket;
declare let canvas: any;
declare let keyboard: any;
declare let game: Game;
declare let ui: {
	notifications: Notifications;
	tables: RollTableDirectory;
	combat: CombatTracker;
	actors: ActorDirectory;
	windows: Record<number, Application>;
};

declare const _templateCache: Record<string, unknown>;

/**
 * The core Game instance which encapsulates the data, settings, and states relevant for managing the game experience.
 * The singleton instance of the Game class is available as the global variable game.
 *
 * @param view		The named view which is active for this game instance.
 * @param data		An object of all the World data vended by the server when the client first connects
 * @param sessionId	The ID of the currently active client session retrieved from the browser cookie
 * @param socket	The open web-socket which should be used to transact game-state data
 */
declare class Game {
	/**
	 * The named view which is currently active.
	 * Game views include: join, setup, players, license, game, stream
	 */
	view: string;

	/** The object of world data passed from the server */
	data: any;

	/** Localization support */
	i18n: Localization;

	/** The Keyboard Manager */
	keyboard: KeyboardManager;

	/** A mapping of installed modules */
	modules: Map<any, any>;

	/** The user role permissions setting */
	permissions: any;

	/** The client session id which is currently active */
	sessionId: string;

	/** Client settings which are used to configure application behavior */
	settings: ClientSettings;

	/** A reference to the open Socket.io connection */
	socket: SocketIOClient.Socket | WebSocket;

	/**
	 * A singleton GameTime instance which manages the progression of time within the game world.
	 * @type {GameTime}
	 */
	time: any;

	/** The id of the active game user */
	userId: string;

	/** A singleton instance of the Audio Helper class */
	audio: AudioHelper;

	/** A singleton instance of the Video Helper class */
	video: VideoHelper;

	/** Whether the Game is running in debug mode */
	debug: boolean;

	/**
	 * A flag for whether texture assets for the game canvas are currently loading
	 */
	loading: boolean;

	/** A flag for whether the Game has successfully reached the "ready" hook */
	ready: boolean;

	/* -------------------------------------------- */
	/*  Entities
	/* -------------------------------------------- */

	users: Users;
	messages: Messages;
	scenes: Scenes;
	actors: Actors;
	items: Items;
	journal: Journal;
	macros: Macros;
	playlists: Playlists;
	combats: CombatEncounters;
	tables: RollTables;
	folders: Folders;
	packs: Collection<any>;

	constructor(
		view: string,
		data: any,
		sessionId: string,
		socket: SocketIOClient.Socket | WebSocket
	);

	/**
	 * Fetch World data and return a Game instance
	 * @return {Promise}	A Promise which resolves to the created Game instance
	 */
	static create(): Promise<Game>;

	/**
	 * Establish a live connection to the game server through the socket.io URL
	 * @param sessionId	The client session ID with which to establish the connection
	 * @return			A promise which resolves to the connected socket, if successful
	 */
	static connect(): Promise<any>;

	/**
	 * Retrieve the cookies which are attached to the client session
	 * @return {Object}	The session cookies
	 */
	static getCookies(): any;

	/**
	 * Get the current World status upon initial connection.
	 */
	static getWorldStatus(
		socket: SocketIOClient.Socket | WebSocket
	): Promise<boolean>;

	/**
	 * Request World data from server and return it
	 */
	static getWorldData(socket: SocketIOClient.Socket): Promise<any>;

	/**
	 * Request setup data from server and return it
	 */
	static getSetupData(socket: SocketIOClient.Socket): Promise<any>;

	/**
	 * Initialize the Game for the current window location
	 */
	initialize(): Promise<void>;

	/**
	 * Display certain usability error messages which are likely to result in the player having a bad experience.
	 */
	protected _displayUsabilityErrors(): void;

	/**
	 * Shut down the currently active Game. Requires GameMaster user permission.
	 * @return	A Promise which resolves to the response object from the server
	 */
	shutDown(): Promise<any>;

	/**
	 * Fully set up the game state, initializing Entities, UI applications, and the Canvas
	 */
	setupGame(): Promise<void>;

	/**
	 * Initialize game state data by creating Collections for all Entity types
	 */
	initializeEntities(): void;

	/**
	 * Initialize the Compendium packs which are present within this Game
	 * Create a Collection which maps each Compendium pack using it's collection ID
	 */
	initializePacks(config: any): Promise<any>;

	/**
	 * Initialize the WebRTC implementation
	 */
	initializeRTC(): Promise<boolean>;

	/**
	 * Initialize core UI elements
	 */
	initializeUI(): void;

	/**
	 * Initialize the game Canvas
	 */
	initializeCanvas(): Promise<void>;

	/**
	 * Ensure that necessary fonts have loaded and are ready for use
	 * Enforce a maximum timeout in milliseconds.
	 * Proceed with rendering after that point even if fonts are not yet available.
	 * @param ms	The timeout to delay
	 */
	protected _checkFontsReady(ms: number): Promise<void>;

	/**
	 * Initialize Keyboard and Mouse controls
	 */
	initializeKeyboard(): void;

	/**
	 * Register core game settings
	 */
	registerSettings(): void;

	/* -------------------------------------------- */
	/*  Properties                                  */
	/* -------------------------------------------- */

	/**
	 * Is the current session user authenticated as an application administrator?
	 */
	get isAdmin(): boolean;

	/**
	 * The currently connected User
	 */
	get user(): User;

	/**
	 * Metadata regarding the current game World
	 */
	get world(): any;

	/**
	 * Metadata regarding the game System which powers this World
	 */
	get system(): any;

	/**
	 * A convenience accessor for the currently active Combat encounter
	 */
	get combat(): Combat;

	/**
	 * A state variable which tracks whether or not the game session is currently paused
	 */
	get paused(): boolean;

	/**
	 * A convenient reference to the currently active canvas tool
	 */
	get activeTool(): string;

	/* -------------------------------------------- */
	/*  Methods                                     */
	/* -------------------------------------------- */

	/**
	 * Toggle the pause state of the game
	 * Trigger the `pauseGame` Hook when the paused state changes
	 * @param pause	The new pause state
	 * @param push	Push the pause state change to other connected clients?
	 */
	togglePause(pause: boolean, push?: boolean): void;

	/**
	 * Log out of the game session by returning to the Join screen
	 */
	logOut(): void;

	/* -------------------------------------------- */
	/*  Socket Listeners and Handlers               */
	/* -------------------------------------------- */

	/**
	 * Open socket listeners which transact game state data
	 */
	openSockets(): void;

	/**
	 * General game-state socket listeners and event handlers
	 */
	static socketListeners(socket: SocketIOClient.Socket | WebSocket): void;

	/**
	 * Activate Event Listeners which apply to every Game View
	 */
	activateListeners(): void;

	/**
	 * On left mouse clicks, check if the element is contained in a valid hyperlink and open it in a new tab.
	 */
	protected _onClickHyperlink(event: MouseEvent): void;

	// Added so developers can easily add system/module specific stuff to the game object
	[key: string]: any;
}
