declare namespace Application {
	interface Options extends Object {
		/** A named "base application" which generates an additional hook */
		baseApplication?: string;
		/** The default pixel width for the rendered HTML */
		width?: number | string;
		/** The default pixel height for the rendered HTML */
		height?: number | string;
		/** The default offset-top position for the rendered HTML */
		top?: number;
		/** The default offset-left position for the rendered HTML */
		left?: number;
		/** Whether to display the application as a pop-out container */
		popOut?: boolean;
		/** Whether the rendered application can be minimized (popOut only) */
		minimizable?: boolean;
		/** Whether the rendered application can be drag-resized (popOut only) */
		resizable?: boolean;
		/** The default CSS id to assign to the rendered HTML */
		id?: string;
		/** An array of CSS string classes to apply to the rendered HTML */
		classes?: string[];

		dragDrop?: DragDrop[];
		/** Track Tab navigation handlers which are active for this Application */
		tabs?: TabV2Options[];
		/** A default window title string (popOut only) */
		title?: string;
		/** The default HTML template path to render for this Application */
		template?: string;
		/**
		 * A list of unique CSS selectors which target containers that should
		 * have their vertical scroll positions preserved during a re-render.
		 */
		scrollY?: string[];
	}

	interface RenderOptions extends Object {
		/** The left positioning attribute */
		left?: number;
		/** The top positioning attribute */
		top?: number;
		/** The rendered width */
		width?: number;
		/** The rendered height */
		height?: number;
		/** The rendered transformation scale */
		scale?: number;
		/** Whether to display a log message that the Application was rendered */
		log?: boolean;
		/** A context-providing string which suggests what event triggered the render */
		renderContext?: string;
		/** The data change which motivated the render request */
		renderData?: any;
	}

	interface HeaderButton {
		label: string;
		class: string;
		icon: string;
		onclick: Function | null;
	}

	interface PositionParam {
		width?: number | 'auto';
		height?: number | 'auto';
		left?: number;
		top?: number;
		scale?: number;
	}

	interface Position {
		width: number;
		height: number;
		left: number;
		top: number;
		scale: number;
	}

	enum RENDER_STATES {
		CLOSING = -2,
		CLOSED = -1,
		NONE = 0,
		RENDERING = 1,
		RENDERED = 2,
		ERROR = 3,
	}
}

declare const MIN_WINDOW_WIDTH: number, MIN_WINDOW_HEIGHT: number;
declare let _appId: number;
declare let _maxZ: number;

/**
 * The standard application window that is rendered for a large variety of UI elements in Foundry VTT.
 * @interface
 *
 * @param options					Configuration options which control how the application is rendered.
 *									Application subclasses may add additional supported options, but the
 *									following configurations are supported for all Applications. The values
 *									passed to the constructor are combined with the defaultOptions defined
 *									at the class level.
 * @param options.baseApplication	A named "base application" which generates an additional hook
 * @param options.width				The default pixel width for the rendered HTML
 * @param options.height			The default pixel height for the rendered HTML
 * @param options.top				The default offset-top position for the rendered HTML
 * @param options.left				The default offset-left position for the rendered HTML
 * @param options.popOut			Whether to display the application as a pop-out container
 * @param options.minimizable		Whether the rendered application can be minimized (popOut only)
 * @param options.resizable			Whether the rendered application can be drag-resized (popOut only)
 * @param options.id				The default CSS id to assign to the rendered HTML
 * @param options.classes			An array of CSS string classes to apply to the rendered HTML
 * @param options.title				A default window title string (popOut only)
 * @param options.template			The default HTML template path to render for this Application
 * @param options.scrollY			A list of unique CSS selectors which target containers that should
 *									have their vertical scroll positions preserved during a re-render.
 *
 * Hooks:
 *		renderApplication
 *		closeApplication
 *		getApplicationHeaderButtons
 *
 * @template D	The schema type of the data used to render the inner template.
 */
declare class Application {
	/**
	 * The options provided to this application upon initialization
	 */
	options: Application.Options;

	/**
	 * The application ID is a unique incrementing integer which is used to identify every application window
	 * drawn by the VTT
	 */
	appId: number;

	/**
	 * Track the current position and dimensions of the Application UI
	 */
	position: Application.Position;

	/**
	 * An internal reference to the HTML element this application renders
	 */
	protected _element: JQuery;

	/**
	 * DragDrop workflow handlers which are active for this Application
	 */
	protected _dragDrop: DragDrop[];

	/**
	 * Tab navigation handlers which are active for this Application
	 */
	protected _tabs: Tabs[];

	/**
	 * SearchFilter handlers which are active for this Application
	 * @type {SearchFilter[]}
	 */
	protected _searchFilters: any;

	/**
	 * Track whether the Application is currently minimized
	 */
	protected _minimized: boolean;

	/**
	 * Track the render state of the Application
	 * @see {Application.RENDER_STATES}
	 */
	protected _state: Application.RENDER_STATES;

	/**
	 * Track the most recent scroll positions for any vertically scrolling containers
	 */
	protected _scrollPositions: any | null;

	constructor(options?: Application.Options);

	/**
	 * Create drag-and-drop workflow handlers for this Application
	 * @return	An array of DragDrop handlers
	 */
	protected _createDragDropHandlers(): DragDrop[];

	/**
	 * Create tabbed navigation handlers for this Application
	 * @return	An array of TabsV2 handlers
	 */
	protected _createTabHandlers(): Tabs[];

	/**
	 * Create search filter handlers for this Application
	 * @return {SearchFilter[]}  An array of SearchFilter handlers
	 */
	protected _createSearchFilters(): any[];

	/**
	 * Assign the default options which are supported by all Application classes.
	 * Application subclasses may include additional options which are specific to their usage.
	 * All keys are optional, descriptions and default values are listed below:
	 */
	static get defaultOptions(): Application.Options;

	/**
	 * Return the CSS application ID which uniquely references this UI element
	 */
	get id(): string;

	/**
	 * Return the active application element, if it currently exists in the DOM
	 */
	get element(): JQuery | HTMLElement;

	/**
	 * The path to the HTML template file which should be used to render the inner content of the app
	 */
	get template(): string;

	/**
	 * Control the rendering style of the application. If popOut is true, the application is rendered in its own
	 * wrapper window, otherwise only the inner app content is rendered
	 */
	get popOut(): boolean;

	/**
	 * Return a flag for whether the Application instance is currently rendered
	 */
	get rendered(): boolean;

	/**
	 * An Application window should define its own title definition logic which may be dynamic depending on its data
	 */
	get title(): string;

	/* -------------------------------------------- */
	/* Application rendering
	/* -------------------------------------------- */

	/**
	 * An application should define the data object used to render its template.
	 * This function may either return an Object directly, or a Promise which resolves to an Object
	 * If undefined, the default implementation will return an empty object allowing only for rendering of static HTML
	 */
	getData(options?: object): any | Promise<any>;

	/**
	 * Render the Application by evaluating it's HTML template against the object of data provided by the getData method
	 * If the Application is rendered as a pop-out window, wrap the contained HTML in an outer frame with window controls
	 *
	 * @param force		Add the rendered application to the DOM if it is not already present. If false, the
	 *					Application will only be re-rendered if it is already present.
	 * @param options	Additional rendering options which are applied to customize the way that the Application
	 *					is rendered in the DOM.
	 *
	 * @param options.left			The left positioning attribute
	 * @param options.top			The top positioning attribute
	 * @param options.width			The rendered width
	 * @param options.height		The rendered height
	 * @param options.scale			The rendered transformation scale
	 * @param options.log			Whether to display a log message that the Application was rendered
	 * @param options.renderContext	A context-providing string which suggests what event triggered the render
	 * @param options.renderData	The data change which motivated the render request
	 *
	 */
	render(force?: boolean, options?: Application.RenderOptions): Application;

	/**
	 * An asynchronous inner function which handles the rendering of the Application
	 * @param options	Provided rendering options, see the render function for details
	 */
	protected _render(force?: boolean, options?: any): Promise<void>;

	/**
	 * Return the inheritance chain for this Application class up to (and including) it's base Application class.
	 */
	private static _getInheritanceChain(): Application[];

	/**
	 * Persist the scroll positions of containers within the app before re-rendering the content
	 * @param html		The HTML object being traversed
	 * @param selectors	CSS selectors which designate elements to save
	 */
	protected _saveScrollPositions(
		html: HTMLElement | JQuery,
		selectors: string[]
	): void;

	/**
	 * Restore the scroll positions of containers within the app after re-rendering the content
	 * @param html		The HTML object being traversed
	 * @param selectors	CSS selectors which designate elements to restore
	 * @private
	 */
	protected _restoreScrollPositions(
		html: HTMLElement | JQuery,
		selectors: string[]
	): void;

	/**
	 * Render the outer application wrapper
	 * @return	A promise resolving to the constructed jQuery object
	 */
	protected _renderOuter(options: any): Promise<JQuery | HTMLElement>;

	/**
	 * Render the inner application content
	 * @param data	The data used to render the inner template
	 * @return		A promise resolving to the constructed jQuery object
	 */
	protected _renderInner(
		data: any,
		options: any
	): Promise<JQuery | HTMLElement>;

	/**
	 * Customize how inner HTML is replaced when the application is refreshed
	 * @param element	The original HTML element
	 * @param html		New updated HTML
	 */
	protected _replaceHTML(
		element: JQuery | HTMLElement,
		html: JQuery | HTMLElement,
		options: any
	): void;

	/**
	 * Customize how a new HTML Application is added and first appears in the DOC
	 */
	protected _injectHTML(html: JQuery | HTMLElement, options: any): void;

	/**
	 * Specify the set of config buttons which should appear in the Application header.
	 * Buttons should be returned as an Array of objects.
	 * The header buttons which are added to the application can be modified by the getApplicationHeaderButtons hook.
	 * @fires Application#hook:getApplicationHeaderButtons
	 */
	protected _getHeaderButtons(): Application.HeaderButton[];

	/* -------------------------------------------- */
	/* Event Listeners and Handlers
	/* -------------------------------------------- */

	/**
	 * Once the HTML for an Application has been rendered, activate event listeners which provide interactivity for
	 * the application
	 */
	protected activateListeners(html: JQuery | HTMLElement): void;

	/**
	 * Handle changes to the active tab in a configured Tabs controller
	 * @param event		A left click event
	 * @param tabs		The TabsV2 controller
	 * @param active	The new active tab name
	 */
	protected _onChangeTab(event: MouseEvent, tabs: Tabs, active: string): void;

	/**
	 * Handle changes to search filtering controllers which are bound to the Application
	 * @param event	The key-up event from keyboard input
	 * @param query	The regular expression to test against
	 * @param html	The HTML element which should be filtered
	 * @private
	 */
	protected _onSearchFilter(
		event: KeyboardEvent,
		query: RegExp,
		html: JQuery | HTMLElement
	): void;

	/**
	 * Define whether a user is able to begin a dragstart workflow for a given drag selector
	 * @param selector	The candidate HTML selector for dragging
	 * @return			Can the current user drag this selector?
	 */
	protected _canDragStart(selector: string): boolean;

	/**
	 * Define whether a user is able to conclude a drag-and-drop workflow for a given drop selector
	 * @param selector	The candidate HTML selector for the drop target
	 * @return			Can the current user drop on this selector?
	 */
	protected _canDragDrop(selector: string): boolean;

	/**
	 * Callback actions which occur at the beginning of a drag start workflow.
	 * @param event	The originating DragEvent
	 */
	protected _onDragStart(event: DragEvent): void;

	/**
	 * Callback actions which occur when a dragged element is over a drop target.
	 * @param event	The originating DragEvent
	 */
	protected _onDragOver(event: DragEvent): void;

	/**
	 * Callback actions which occur when a dragged element is dropped on a target.
	 * @param event	The originating DragEvent
	 */
	protected _onDrop(event: DragEvent): void;

	/* -------------------------------------------- */
	/*  Methods                                     */
	/* -------------------------------------------- */

	/**
	 * Bring the application to the top of the rendering stack
	 */
	bringToTop(): void;

	/**
	 * Close the application and un-register references to it within UI mappings
	 * This function returns a Promise which resolves once the window closing animation concludes
	 */
	close(): Promise<void>;

	/**
	 * Minimize the pop-out window, collapsing it to a small tab
	 * Take no action for applications which are not of the pop-out variety or apps which are already minimized
	 * @return	A Promise which resolves once the minimization action has completed
	 */
	minimize(): Promise<void>;

	/**
	 * Maximize the pop-out window, expanding it to its original size
	 * Take no action for applications which are not of the pop-out variety or are already maximized
	 * @return	A Promise which resolves once the maximization action has completed
	 */
	maximize(): Promise<void>;

	/**
	 * Set the application position and store it's new location.
	 *
	 * @param left		The left offset position in pixels
	 * @param top		The top offset position in pixels
	 * @param width		The application width in pixels
	 * @param height	The application height in pixels
	 * @param scale		The application scale as a numeric factor where 1.0 is default
	 *
	 * @returns	The updated position object for the application containing the new values
	 */
	setPosition({
		left,
		top,
		width,
		height,
		scale,
	}: Application.PositionParam): Application.Position;

	/**
	 * Handle application minimization behavior - collapsing content and reducing the size of the header
	 */
	protected _onToggleMinimize(ev: Event | JQuery.Event): void;

	/**
	 * Additional actions to take when the application window is resized
	 */
	protected _onResize(event: Event | JQuery.Event): void;
}
