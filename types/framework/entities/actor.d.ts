declare namespace Actor {
	/**
	 * Data structure used by all Actors.
	 */
	interface Data<T = {}> extends Entity.Data<T> {
		img: string;
		items: Item.Data[];
		effects: ActiveEffect[];
		token: any;
	}

	interface Options extends Entity.Options {
		token: Token;
	}
}

/**
 * The Collection of Actor entities.
 *
 * @see {@link Actor} The Actor entity.
 * @see {@link ActorDirectory} All Actors which exist in the world are rendered within the ActorDirectory sidebar tab.
 *
 * @example <caption>Retrieve an existing Actor by its id</caption>
 * let actor = game.actors.get(actorId);
 */
declare class Actors extends EntityCollection<Actor, Actor.Data> {
	/**
	 * A mapping of synthetic Token Actors which are currently active within the viewed Scene.
	 * Each Actor is referenced by the Token.id.
	 * @type {Object}
	 */
	tokens: any;

	/* -------------------------------------------- */
	/*  Sheet Registration Methods                  */
	/* -------------------------------------------- */

	/**
	 * Register an Actor sheet class as a candidate which can be used to display Actors of a given type
	 * See EntitySheetConfig.registerSheet for details
	 *
	 * @example <caption>Register a new ActorSheet subclass for use with certain Actor types.</caption>
	 * Actors.registerSheet("dnd5e", ActorSheet5eCharacter, { types: ["character"], makeDefault: true });
	 */
	static registerSheet(
		scope: string,
		sheetClass: typeof Application,
		{
			label,
			types,
			makeDefault,
		}?: { label?: string; types?: string[]; makeDefault?: boolean }
	): void;

	/**
	 * Unregister an Actor sheet class, removing it from the list of avaliable sheet Applications to use
	 * See EntitySheetConfig.unregisterSheet for details
	 */
	static unregisterSheet(
		scope: string,
		sheetClass: typeof Application,
		{ types }?: { types?: string[] }
	): void;

	/**
	 * Return an Array of currently registered sheet classes for this Entity type
	 */
	static get registeredSheets(): ActorSheet[];
}

/**
 * The Actor Entity which represents the protagonists, characters, enemies, and more that inhabit and take actions
 * within the World.
 *
 * @see {@link Actors} Each Actor belongs to the Actors collection.
 * @see {@link ActorSheet} Each Actor is edited using the ActorSheet application or a subclass thereof.
 * @see {@link ActorDirectory} All Actors which exist in the world are rendered within the ActorDirectory sidebar tab.
 *
 *
 * @example <caption>Create a new Actor</caption>
 * let actor = await Actor.create({
 *   name: "New Test Actor",
 *   type: "character",
 *   img: "artwork/character-profile.jpg",
 *   folder: folder.data._id,
 *   sort: 12000,
 *   data: {},
 *   token: {},
 *   items: [],
 *   flags: {}
 * });
 *
 * @example <caption>Retrieve an existing Actor</caption>
 * let actor = game.actors.get(actorId);
 */
declare class Actor<T = {}, I extends Item = Item> extends Entity<T> {
	data: Actor.Data<T>;

	/**
	 * A reference to a placed Token which creates a synthetic Actor
	 */
	token: Token;

	/**
	 * Construct the Array of Item instances for the Actor
	 * Items are prepared by the Actor.prepareEmbeddedEntities() method
	 */
	items: Collection<I>;

	/**
	 * Construct the Array of ActiveEffect instances for the Actor
	 * ActiveEffects are prepared by the Actor.prepareEmbeddedEntities() method
	 */
	effects: Collection<ActiveEffect>;

	/**
	 * A set that tracks which keys in the data model were modified by active effects
	 */
	overrides: unknown;

	/**
	 * Cache an Array of allowed Token images if using a wildcard path
	 * @private
	 */
	private _tokenImages: string[];

	static get config(): Entity.Config<Actor>;

	/**
	 * First prepare any derived data which is actor-specific and does not depend on Items
	 * or Active Effects
	 */
	prepareBaseData(): void;

	/** @override */
	prepareEmbeddedEntities(): void;

	/**
	 * Apply final transformations to the Actor data after all effects have been applied
	 */
	prepareDerivedData(): void;
	
	/* -------------------------------------------- */
	/*  Properties                                  */
	/* -------------------------------------------- */

	/**
	 * A convenient reference to the file path of the Actor's profile image
	 */
	get img(): string;

	/**
	 * Classify Owned Items by their type
	 */
	get itemTypes(): { [key: string]: I[] };

	/**
	 * Test whether an Actor entity is a synthetic representation of a Token (if true) or a full Entity (if false)
	 */
	get isToken(): boolean;

	/**
	 * An array of ActiveEffect instances which are present on the Actor which have a limited duration.
	 */
	get temporaryEffects(): ActiveEffect[];

	/** @override */
	prepareData(): void;

	/**
	 * First prepare any derived data which is actor-specific and does not depend on Items or Active Effects
	 */
	prepareBaseData(): void;

	/**
	 * Apply final transformations to the Actor data after all effects have been applied
	 */
	prepareDerivedData(): void;

	/** @override */
	prepareEmbeddedEntities(): void;

	/**
	 * Prepare a Collection of OwnedItem instances which belong to this Actor.
	 * @param  items	The raw array of item objects
	 * @return			The prepared owned items collection
	 */
	protected _prepareOwnedItems(items: Item.Data[]): Collection<I>;

	/**
	 * Prepare a Collection of ActiveEffect instances which belong to this Actor.
	 * @param effects	The raw array of active effect objects
	 * @return			The prepared active effects collection
	 */
	protected _prepareActiveEffects(
		effects: object[]
	): Collection<ActiveEffect>;

	/**
	 * Apply any transformations to the Actor data which are caused by ActiveEffects.
	 */
	applyActiveEffects(): void;

	/* -------------------------------------------- */
	/*  Methods                                     */
	/* -------------------------------------------- */

	/**
	 * Create a synthetic Actor using a provided Token instance
	 * If the Token data is linked, return the true Actor entity
	 * If the Token data is not linked, create a synthetic Actor using the Token's actorData override
	 * @param token
	 */
	static fromToken(token: Token): Actor;

	/**
	 * Create a synthetic Token Actor instance which is used in place of an actual Actor.
	 * Cache the result in Actors.tokens.
	 * @param baseActor
	 * @param token
	 */
	static createTokenActor(baseActor: Actor, token: Token): Actor;

	/**
	 * Retrieve an Array of active tokens which represent this Actor in the current canvas Scene.
	 * If the canvas is not currently active, or there are no linked actors, the returned Array will be empty.
	 *
	 * @param linked	Only return tokens which are linked to the Actor. Default (false) is to return all
	 *					tokens even those which are not linked.
	 *
	 * @return			An array of tokens in the current Scene which reference this Actor.
	 */
	getActiveTokens(linked?: boolean): Token[];

	/**
	 * Prepare a data object which defines the data schema used by dice roll commands against this Actor
	 */
	getRollData(): Record<string, any>;

	/**
	 * Get an Array of Token images which could represent this Actor
	 */
	getTokenImages(): Promise<string[]>;

	/**
	 * Handle how changes to a Token attribute bar are applied to the Actor.
	 * This allows for game systems to override this behavior and deploy special logic.
	 * @param attribute	The attribute path
	 * @param value		The target attribute value
	 * @param isDelta	Whether the number represents a relative change (true) or an absolute change (false)
	 * @param isBar		Whether the new value is part of an attribute bar, or just a direct value
	 */
	modifyTokenAttributes(
		attribute: string,
		value: number,
		isDelta?: boolean,
		isBar?: boolean
	): Promise<Actor>;

	/**
	 * Roll initiative for all Combatants in the currently active Combat encounter which are associated with this Actor.
	 * If viewing a full Actor entity, all Tokens which map to that actor will be targeted for initiative rolls.
	 * If viewing a synthetic Token actor, only that particular Token will be targeted for an initiative roll.
	 *
	 * @param options					Configuration for how initiative for this Actor is rolled.
	 * @param options.createCombatants	Create new Combatant entries for Tokens associated with this actor.
	 * @param options.rerollInitiative	Re-roll the initiative for this Actor if it has already been rolled.
	 * @param options.initiativeOptions	Additional options passed to the Combat#rollInitiative method.
	 * @return							A promise which resolves to the Combat entity once rolls are complete.
	 */
	rollInitiative({
		createCombatants,
		rerollInitiative,
		initiativeOptions,
	}?: {
		createCombatants?: boolean;
		rerollInitiative?: boolean;
		initiativeOptions?: object;
	}): Promise<Combat | null>;

	/**
	 * When Owned Items are created process each item and extract Active Effects to transfer to the Actor.
	 * @param created	Created owned Item data objects
	 * @param temporary	Is this a temporary item creation?
	 * @return			An array of effects to transfer to the Actor
	 */
	protected _createItemActiveEffects(
		created: object[],
		{ temporary }?: { temporary?: boolean }
	): any[];

	/**
	 * When Owned Items are created process each item and extract Active Effects to transfer to the Actor.
	 * @param deleted	The array of deleted owned Item data
	 */
	protected _deleteItemActiveEffects(deleted: object[]): Promise<any>;

	/* -------------------------------------------- */
	/* Owned Item Management
	/* -------------------------------------------- */

	/**
	 * Get an Item instance corresponding to the Owned Item with a given id
	 * @param itemId	The owned Item id to retrieve
	 * @return			An Item instance representing the Owned Item within the Actor entity
	 */
	getOwnedItem(itemId: string): I | null;

	/**
	 * Create a new item owned by this Actor. This redirects its arguments to the createEmbeddedEntity method.
	 * @see {Entity#createEmbeddedEntity}
	 *
	 * @param itemData				Data for the newly owned item
	 * @param options				Item creation options
	 * @param options.renderSheet	Render the Item sheet for the newly created item data
	 * @return						A Promise resolving to the created Owned Item data
	 */
	createOwnedItem(itemData: object, options?: object): Promise<any>;

	/**
	 * Update an owned item using provided new data. This redirects its arguments to the updateEmbeddedEntity method.
	 * @see {Entity#updateEmbeddedEntity}
	 *
	 * @param itemData	Data for the item to update
	 * @param options	Item update options
	 * @return			A Promise resolving to the updated Owned Item data
	 */
	updateOwnedItem(itemData: object, options?: object): Promise<any>;

	/**
	 * Delete an owned item by its id. This redirects its arguments to the deleteEmbeddedEntity method.
	 * @see {Entity#deleteEmbeddedEntity}
	 *
	 * @param itemId	The ID of the item to delete
	 * @param options	Item deletion options
	 * @return			A Promise resolving to the deleted Owned Item data
	 */
	deleteOwnedItem(itemId: string, options?: object): Promise<any>;

	/* -------------------------------------------- */
	/*  DEPRECATED                                  */
	/* -------------------------------------------- */

	/**
	 * @deprecated since 0.7.0
	 */
	importItemFromCollection(collection: string, entryId: string): I;

	/**
	 * @deprecated since 0.7.2
	 * @see {@link Entity#hasPlayerOwner}
	 */
	get isPC(): boolean;
}
